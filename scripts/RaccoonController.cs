﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaccoonController : NPC
{

    public Transform rightHandTarget;
    public Transform leftHandTarget;
    public Transform holeTarget;
    public GameObject sticker;

    private Vector3 rightTargetInitPos;
    private Vector3 leftTargetInitPos;

    private Collider2D leftHand;
    private Collider2D rightHand;

    void Start()
    {
        rightTargetInitPos = rightHandTarget.position;
        leftTargetInitPos = leftHandTarget.position;

        rightHand = transform.Find("LeftArm/ArmJoint0/ArmJoint1/ArmHand").gameObject.GetComponent<Collider2D>();
        leftHand = transform.Find("RightArm/ArmJoint0/ArmJoint1/ArmHand").gameObject.GetComponent<Collider2D>();
    }

    protected override void NPCSpecificUpdate()
    {

        if (holdingFruit != null)
        {
            leftHandTarget.position = Vector2.MoveTowards(leftHandTarget.position, holeTarget.position, Time.deltaTime * 2);
            if (leftHandTarget.position == holeTarget.position)
            {
                // *Play eating noise and particles*
                Destroy(holdingFruit);
                holdingFruit = null;

                progressDialog(4, 5);
                displayDialogue();

                sticker.SetActive(true);

                StartCoroutine(provideReward());
            }
            // Don't bother with anything else until we have eaten this fruit!
            return;
        }



        // Find nearby fruit and reach for them. All NPCs should search for objects in the Fruit layer
        // But specific NPCs will differ in the TAG of the object they are searching for
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, searchRadius, LayerMask.GetMask("Fruit"));

        // Finding closest collider using code from here: http://answers.unity.com/answers/240566/view.html
        Collider2D closestCollider = null;
        foreach (Collider2D hit in hitColliders)
        {
            //if (!hit.gameObject.CompareTag(tagOfFruitIWant)) continue;
            if (!hit.gameObject.name.Contains(tagOfFruitIWant)) continue;
            if (closestCollider == null)
                closestCollider = hit;
            if (Vector3.Distance(transform.position, hit.transform.position) <= Vector3.Distance(transform.position, closestCollider.transform.position))
            {
                closestCollider = hit;
            }
        }
        // No nearby fruit
        if (closestCollider == null)
            leftHandTarget.position = Vector2.MoveTowards(leftHandTarget.position, leftTargetInitPos, Time.deltaTime * 4);
        else
        {
            GameObject closest = closestCollider.gameObject;
            Vector3 closestPos = closest.transform.position;
            leftHandTarget.position = Vector2.MoveTowards(leftHandTarget.position, closestPos, Time.deltaTime * 10);

            if (leftHand.IsTouching(closestCollider))
            {
                // Change tag so that the player doesn't immediately steal it back
                closest.tag = "AcceptedByNPC";
                closest.transform.SetParent(leftHand.gameObject.transform);
                closest.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                closest.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                closest.GetComponent<Rigidbody2D>().angularVelocity = 0;
                closest.GetComponent<Rigidbody2D>().gravityScale = 0;
                // closest.transform.localRotation = transform.rotation;
                closest.GetComponent<Collider2D>().enabled = false;

                holdingFruit = closest;
            }
        }
    }
}
