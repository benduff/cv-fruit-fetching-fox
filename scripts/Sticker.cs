﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sticker : MonoBehaviour
{
    private int maxStickers = 7;

    public GameObject player;
    public GameObject stickerUI;
    public GameObject stickerMessage;

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        collectSticker(collision);
    }

    private void collectSticker(Collider2D collision)
    {
        if (collision.gameObject == player)
        {

            if (!StickerBook.collectedStickers.Contains(name))
            {
                StickerBook.collectedStickers.Add(name);
            }

            //show collected message
            StartCoroutine(DisplayMessage(name));

            //show sticker in sticker book
            stickerUI.GetComponent<Image>().color = new Color(1, 1, 1, 1);

        }
    }

    IEnumerator DisplayMessage(string sticker)     
    {
        //destory sprite renderer (cant set object inactive while coroutine running)
        Destroy(gameObject.GetComponent<SpriteRenderer>());

        //set+show message
        stickerMessage.GetComponent<Text>().text = "You collected the " + this.name + " sticker!";
        stickerMessage.SetActive(true);

        //wait 3 seconds
        yield return new WaitForSeconds(3f);

        //if all stickers have now been collected congratulate player
        if (StickerBook.collectedStickers.Count == maxStickers)
        {
            stickerMessage.GetComponent<Text>().text =
                "Congratulations, you completed your sticker collection!";

            //wait 3 seconds
            yield return new WaitForSeconds(5f);
        }

        //set sticker and message inactive
        stickerMessage.SetActive(false);
        gameObject.SetActive(false);
    }

}
