﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour
{
    public Rigidbody2D body;
    public Transform springPoint;
    public bool rightHand = true;
    private int button;
    private SimpleIK IKScript;

    public GameObject otherHand;
    private GameObject otherHandItem;
    private GameObject prevHeldItem = null;
    private GameObject shootItem = null;
    private bool aimingSlingshot = false;
    Vector2 shootItemPos, shooterPos;
    private float throwSpeed = 6;
    private string prevTag;

    private List<string> shootables = new List<string>() { "Fruit", "Flowers" };

    private Transform targetTransform;

    private bool mouseDown = false;
    private GameObject heldItem = null;

    private Vector3 lastPos;

    private List<string> grabbables;

    private Vector2 lastTartgetPos;

    void Start()
    {
        // This script (handController) is attached to the hand, but we want to get the target of this arm, so get lots of parents
        IKScript = transform.parent.parent.parent.GetComponent<SimpleIK>();
        targetTransform = IKScript.getTarget;
        // Convert bool to int for ease of use in script
        button = rightHand ? 1 : 0;
        grabbables = transform.parent.parent.parent.parent.GetComponent<CreatureController>().layersICanGrab;


        //Creating list of points for slingshot trajectory
        trajectoryPoints = new List<GameObject>();
        //   TrajectoryPoints are instatiated
        for (int i = 0; i < numOfTrajectoryPoints; i++)
        {
            GameObject dot = (GameObject)Instantiate(TrajectoryPointPrefeb);
            dot.GetComponent<Renderer>().enabled = false;
            trajectoryPoints.Insert(i, dot);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Speed of the player
        float actualSpeed = Mathf.Abs(body.velocity.magnitude);

        // How fast the hand moves towards it's goal
        float armSpeed = Mathf.Max(1, (4 + actualSpeed * 6) * Time.deltaTime);

        // How fast the player gets pulled towards things like ladders
        float pullSpeed = Mathf.Max(1, 10 * Time.deltaTime);


        if (Input.GetKey("left shift") 
            && heldItem != null 
            && LayerMask.LayerToName(heldItem.layer) == "Slingshot")
        {
            // slingshot arm movement needs to stop first (force this)
            UseSlingshot();

            targetTransform.position = lastTartgetPos;

        }
        // otherwise move arms as normal:
        else
        {
            if (Input.GetMouseButton(button))
            {
                mouseDown = true;
                Vector2 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
                Vector2 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
                targetTransform.position = Vector2.MoveTowards(targetTransform.position, curPosition, armSpeed);

                lastTartgetPos = targetTransform.position;
            }
            // If the mouse is not pressed:
            else
            {
                // Move hand back to default position on hips
                targetTransform.position = Vector2.MoveTowards(targetTransform.position, springPoint.position, armSpeed);
                mouseDown = false;
            }

            if (aimingSlingshot == true)
            {
                aimingSlingshot = false;
                DisableTP();
            }
        }

        // What should happen when you let go of the mouse (drop/shoot item etc)
        if (Input.GetMouseButtonUp(button) && heldItem != null && !heldItem.CompareTag("AcceptedByNPC"))
        {
            heldItem.tag = prevTag;
            heldItem.GetComponent<Rigidbody2D>().gravityScale = 1;
            heldItem.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            heldItem.GetComponent<Collider2D>().enabled = true;
            heldItem.transform.SetParent(null);

            if (LayerMask.LayerToName(heldItem.layer) == "Slingshot")
            {
                aimingSlingshot = false;
                DisableTP();
            }

            prevHeldItem = heldItem;  //to reference when used with shooter
            heldItem = null;
        }
        lastPos = transform.position;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!mouseDown) return;
        if (heldItem != null) return;
        GameObject other = collision.gameObject;
        if (other != null
            && other.tag != "Holding"
            && grabbables.Contains(LayerMask.LayerToName(other.layer)))
        {
            prevTag = other.tag;
            other.tag = "Holding";
            heldItem = other;
            other.transform.SetParent(transform);
            other.GetComponent<Rigidbody2D>().gravityScale = 0;
            other.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            other.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            other.GetComponent<Rigidbody2D>().angularVelocity = 0;
        }
    }

    public GameObject TrajectoryPointPrefeb;
    private int numOfTrajectoryPoints = 30;
    private List<GameObject> trajectoryPoints;

    //USING SLINGSHOT:
    void UseSlingshot()
    {       
        otherHandItem = otherHand.GetComponent<HandController>().heldItem;

        //if fruit in other hand - load up shooter with fruit
        if (otherHandItem != null
            && (shootables.Contains(LayerMask.LayerToName(otherHandItem.layer))))
        {
            aimingSlingshot = true;

            Vector3 vel = GetVelocity();
            SetTrajectoryPoints(otherHand.transform.position, vel / otherHandItem.GetComponent<Rigidbody2D>().mass);

        }
        //if shooter is loaded and fruit is released - shoot
        if (otherHandItem == null
            && aimingSlingshot == true)
        {
            // get positions to calculate shot direction/strength
            shootItem = otherHand.GetComponent<HandController>().prevHeldItem;
            shootItemPos = shootItem.GetComponent<Transform>().position;
            shooterPos = heldItem.GetComponent<Transform>().position;

            //shoot
            shootItem.GetComponent<Rigidbody2D>().velocity =
                new Vector2((shooterPos.x - shootItemPos.x) * throwSpeed, (shooterPos.y - shootItemPos.y) * throwSpeed);

            //set shooter back to "not loaded"
            aimingSlingshot = false;
            DisableTP();
        }
    }


    //CALCULATING POSITION OF TRAJECTORY POINTS:
    private Vector2 GetVelocity()
    {
        shootItemPos = otherHandItem.GetComponent<Transform>().position;
        shooterPos = heldItem.GetComponent<Transform>().position;
        return new Vector2((shooterPos.x - shootItemPos.x) * throwSpeed, (shooterPos.y - shootItemPos.y) * throwSpeed);
    }
    void SetTrajectoryPoints(Vector3 pStartPosition, Vector3 pVelocity)
    {
        float velocity = Mathf.Sqrt((pVelocity.x * pVelocity.x) + (pVelocity.y * pVelocity.y));
        float angle = Mathf.Rad2Deg * (Mathf.Atan2(pVelocity.y, pVelocity.x));
        float fTime = 0;

        fTime += 0.1f;
        for (int i = 0; i < numOfTrajectoryPoints; i++)
        {
            float dx = velocity * fTime * Mathf.Cos(angle * Mathf.Deg2Rad);
            float dy = velocity * fTime * Mathf.Sin(angle * Mathf.Deg2Rad) - (Physics2D.gravity.magnitude * fTime * fTime / 2.0f);
            Vector3 pos = new Vector3(pStartPosition.x + dx, pStartPosition.y + dy, 2);
            trajectoryPoints[i].transform.position = pos;
            trajectoryPoints[i].GetComponent<Renderer>().enabled = true;
            trajectoryPoints[i].transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(pVelocity.y - (Physics.gravity.magnitude) * fTime, pVelocity.x) * Mathf.Rad2Deg);
            fTime += 0.1f;
        }
    }


    // disable trajectory points (called when changing from aiming to not aiming)
    void DisableTP()
    {
        for (int i = 0; i < numOfTrajectoryPoints; i++)
        {
            trajectoryPoints[i].GetComponent<Renderer>().enabled = false;
        }
    }

}
