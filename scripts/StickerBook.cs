﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickerBook : MonoBehaviour
{
    public static bool bookOpen = false;
    public GameObject bookUI;


    //when sticker is collected name is added to list
    //could be used for checking if a sticker has been collected
    //made it string to easily reference, added string is name of sticker object
    public static List<string> collectedStickers;


    void Start()
    {
        collectedStickers = new List<string>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (bookOpen)
            {
                if (!PauseMenu.paused)
                {
                    // unfreeze game but only if pause menu is not also up
                    Time.timeScale = 1f;  
                }
                bookOpen = false;

                bookUI.SetActive(false);
            }
            else
            {
                Time.timeScale = 0f;    //freeze game
                bookOpen = true;

                bookUI.SetActive(true);
            }
        }
    }

}
