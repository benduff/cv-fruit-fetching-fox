﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public static bool paused = false;
    private bool showControls = false;

    public GameObject pausePanel;
    public GameObject controlsPanel;

    public Button back;
    public Button controls;
    public Button quit;

    void Start()
    {
        controls.onClick.AddListener(ClickControls);
        back.onClick.AddListener(CloseMenu);
        quit.onClick.AddListener(QuitGame);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
            {
                //pause game
                Time.timeScale = 0f;
                paused = true;
                pausePanel.SetActive(true);
            }
            else
            {
                CloseMenu();
            }
        }
    }

    void CloseMenu()
    {
        if (!StickerBook.bookOpen)
        {
            //unpause game so long as sticker book is not up
            Time.timeScale = 1f;
        }
        paused = false;
        pausePanel.SetActive(false);
        showControls = false;
        controlsPanel.SetActive(false);
        
    }

    void ClickControls()
    {
        if (!showControls)
        {
            controlsPanel.SetActive(true);
            showControls = true;
        }
        else
        {
            controlsPanel.SetActive(false);
            showControls = false;
        }
    }

    void QuitGame()
    {
        Application.Quit();
    }

}
