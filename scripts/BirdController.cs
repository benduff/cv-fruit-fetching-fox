﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : NPC
{
    public GameObject egg;
    public GameObject nest;
    public GameObject umbrella;
    public GameObject bird;
    public GameObject sticker;

    private float eggInNest = 2f;   // required min distance for egg to be in nest
    private float birdSpeed = 0.1f;   // speed bird moves to nest

    private bool birdHelped;
    private bool playerHasBeenThanked = false;

    private Vector2 birdNestPos;


    void Start()
    {
        birdNestPos = new Vector2(nest.transform.position.x, nest.transform.position.y + 1.2f);
    }

    protected override void NPCSpecificUpdate()
    {

        // used to check if egg is in nest
        float eggNestDistance = (egg.transform.position - nest.transform.position).magnitude;

        // when egg in nest:
        if (eggNestDistance < eggInNest && !birdHelped)
        {
            if (!playerHasBeenThanked)
            {
                playerHasBeenThanked = true;

                progressDialog(3, 3);
                displayDialogue();

                // bird drops umbrella, can be used by player now
                DropUmbrella();

                //egg.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            }

            // bird moves to nest
            bird.transform.position = Vector2.MoveTowards(bird.transform.position, birdNestPos, birdSpeed);
            

            // could add here: wing movement
            
            if (bird.transform.position == (Vector3)birdNestPos)
            {
                // bird is in nest, quest complete, give sticker
                sticker.SetActive(true);
                birdHelped = true;
            }
        }

    }

    // initially umbrella cannot be grabbed by player as bird is "using" it
    // call this to enable player to grab umbrella
    void DropUmbrella()
    {
        umbrella.tag = "Untagged";
        umbrella.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

}
